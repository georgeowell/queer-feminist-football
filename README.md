![Build Status](https://gitlab.com/pages/mkdocs/badges/master/build.svg)
# Queer Feminist Football Klub

## Why was it setup
Queer Feminist Football Klub is an initiative that begun in Birmingham UK to create a space for marginalised working class people to enjoy the beautiful game.

## Upcoming events
Upcoming events will be announced on the brum [Demosphere](https://demosphere.eu/) calendar [Brum Freed](https://brum.demosphere.eu/).

## Rules
The published rules for the club can be found on the [website](https://georgeowell.gitlab.io/queer-feminist-football).

## Making changes to these rules
If you are not familar with using git or Gitlab, please contact **feministactionbrum at riseup.net** with any feedback or suggestions

This site is hosted using GitLab Pages. You can browse its source code, fork it and start using it on your projects.

For full documentation visit [mkdocs.org](https://www.mkdocs.org/).
